#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pop.hub

if __name__ == "__main__":
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="grains")
    hub.grains.init.cli()
