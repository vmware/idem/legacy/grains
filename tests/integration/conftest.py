import mock
import pytest


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="grains")

    with mock.patch("sys.argv", ["grains"]):
        hub.pop.config.load(
            ["grains", "idem", "acct", "rend"], "grains", parse_cli=True
        )

    hub.grains.init.standalone()

    yield hub
